const path = require('path');
const withImages = require('next-images');

// const withPlugins = require('next-compose-plugins');
// const withOptimizedImages = require('next-optimized-images');

const settings = {
	env: {
		NEXT_APP_EXPIRE_TOKEN_DATE: 365,
		NEXT_APP_TOASTER_CLOSE_TIMEOUT: 5000,
		NEXT_APP_API_URL: 'https://jsonplaceholder.typicode.com',
	},
	enableSvg: true,
	webpack(config) {
		config.module.rules.push(
			{
				test: /\.(jpe?g|png)$/i,
				use: [
					{
						loader: 'responsive-images-loader',
						options: {
							adapter: require('responsive-images-loader/sharp'),
							publicPath: '/_next',
							name: 'static/media/[hash]-[width].[ext]',
						},
					},
				],
			},
			{
				test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'url-loader',
			},
		);

		return config;
	},
};

// module.exports = withImages(settings);
module.exports = settings;
