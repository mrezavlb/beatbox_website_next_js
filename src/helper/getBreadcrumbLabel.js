export const getBreadcrumbLabel = (title, crumbs) => {
	let label = title;
	crumbs &&
		crumbs.forEach(item => {
			if (item.crumb === title) {
				label = item.name;
			}
		});
	return label;
};
