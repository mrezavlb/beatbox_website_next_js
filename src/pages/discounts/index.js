import Head from 'next/head';
import MainLayout from '../../layouts/main';
import { connect } from 'react-redux';
import Widget from '../../components/public/widget';
import Pagination from '../../components/public/pagination';

const Index = props => {
	const {} = props;

	const pageChangeHandler = ({ selected }) => {
		console.log(selected + 1);
	};

	return (
		<div>
			<Head>
				<title>وبسایت بیت باکس | تخفیف ها</title>
				<link rel="icon" href="/img/app_logo.svg" />
			</Head>
			<div className="container">
				<Widget
					cardType="beat"
					data={['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10']}
					title="تخفیف ها"
					moreBtn={{ show: false, content: '', path: '' }}
					isDiscount={true}
				/>
				<div className="d-flex justify-content-center align-items-center pb-4">
					<Pagination totalPage={20} currentPage={1} pageChangeHandler={pageChangeHandler} />
				</div>
			</div>
		</div>
	);
};

const mapStateToProps = state => {
	return {};
};

const mapDispatchToProps = dispatch => {
	return {};
};

Index.Layout = MainLayout;

export default connect(mapStateToProps, mapDispatchToProps)(Index);
