import Head from 'next/head';
import MainLayout from '../../layouts/main';
import { connect } from 'react-redux';

const Index = props => {
	const {} = props;

	return (
		<div>
			<Head>
				<title>وبسایت بیت باکس | قوانین</title>
				<link rel="icon" href="/img/app_logo.svg" />
			</Head>
			قوانین
		</div>
	);
};

const mapStateToProps = state => {
	return {};
};

const mapDispatchToProps = dispatch => {
	return {};
};

Index.Layout = MainLayout;

export default connect(mapStateToProps, mapDispatchToProps)(Index);
