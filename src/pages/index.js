import Head from 'next/head';
import Link from 'next/link';
import { connect } from 'react-redux';
import { initializeStore } from '../store';
import { PostAction } from '../actions';
import MainLayout from '../layouts/main';
import { Main } from '../styles/pages/home';
import Section from '../components/private/home/section';
import Widget from '../components/public/widget';

const Index = props => {
	const { notificationBar } = props;

	return (
		<>
			<Head>
				<title>وبسایت بیت باکس</title>
				<link rel="icon" href="/img/app_logo.svg" />
			</Head>
			<Section withNotification={notificationBar.show} />
			<Main>
				<div className="container">
					<Widget
						cardType="beat"
						data={['0', '1', '2', '3']}
						title="آخرین تخفیف ها"
						moreBtn={{ show: true, content: 'تخفیف های بیشتر', path: '/discounts' }}
						isDiscount={true}
					/>
					<Widget
						cardType="text"
						data={['0', '1', '2', '3']}
						title="آخرین بیت ها"
						moreBtn={{ show: true, content: 'بیت های بیشتر', path: '/beats' }}
						isDiscount={false}
					/>
					<Widget
						cardType="beat"
						data={['0', '1', '2', '3']}
						title="جدید ترین ریمیک ها"
						moreBtn={{ show: true, content: 'ریمیک های بیشتر', path: '/beats/remakes' }}
						isDiscount={false}
					/>
				</div>
			</Main>
		</>
	);
};

export const getServerSideProps = async () => {
	const reduxStore = initializeStore();
	const { dispatch } = reduxStore;
	const postData = {
		page: 1,
		limit: 8,
	};

	// await dispatch(PostAction.getPosts(postData));

	return { props: { initialReduxState: reduxStore.getState() } };
};

const mapStateToProps = state => {
	return {
		notificationBar: state.Global.notificationBar,
	};
};

const mapDispatchToProps = dispatch => {
	return {};
};

Index.Layout = MainLayout;

export default connect(mapStateToProps, mapDispatchToProps)(Index);
