import Head from 'next/head';
import MainLayout from '../../layouts/main';
import { connect } from 'react-redux';
import { useRouter } from 'next/router';
import { useEffect, useRef, useState } from 'react';
import { Ticket } from '../../styles/pages/product';
import Detail from '../../components/private/product/detail';
import Audio from '../../components/private/product/audio';

const ProductId = props => {
	const {} = props;
	const { query } = useRouter();
	const { productId } = query;
	const [isPlaying, setIsPlaying] = useState(false);
	const musicPath = 'https://beatbox.ir/storage/2021/3/29/386/beats/5742/3sPwUYUbJ0.mp3';

	return (
		<div>
			<Head>
				<title>وبسایت بیت باکس | محصول </title>
				<link rel="icon" href="/img/app_logo.svg" />
			</Head>
			<div className="container py-4">
				<Detail productId={productId} />
				<Audio productId={productId} isPlaying={isPlaying} setIsPlaying={setIsPlaying} musicPath={musicPath} />
			</div>
		</div>
	);
};

const mapStateToProps = state => {
	return {};
};

const mapDispatchToProps = dispatch => {
	return {};
};

ProductId.Layout = MainLayout;

export default connect(mapStateToProps, mapDispatchToProps)(ProductId);
