import Head from 'next/head';
import MainLayout from '../../layouts/main';
import { connect } from 'react-redux';
import Widget from '../../components/private/blog/widget';
import Pagination from '../../components/public/pagination';
import Filter from '../../components/private/blog/filter';

const Index = props => {
	const {} = props;

	const pageChangeHandler = ({ selected }) => {
		console.log(selected + 1);
	};

	return (
		<div>
			<Head>
				<title>وبسایت بیت باکس | بلاگ</title>
				<link rel="icon" href="/img/app_logo.svg" />
			</Head>
			<div className="container pt-2">
				<Filter />
				<Widget data={['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10']} />
			</div>
			<div className="d-flex justify-content-center align-items-center pb-4">
				<Pagination totalPage={20} currentPage={1} pageChangeHandler={pageChangeHandler} />
			</div>
		</div>
	);
};

const mapStateToProps = state => {
	return {};
};

const mapDispatchToProps = dispatch => {
	return {};
};

Index.Layout = MainLayout;

export default connect(mapStateToProps, mapDispatchToProps)(Index);
