import Head from 'next/head';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { connect } from 'react-redux';
import { ContainerFluid, MenuBackdrop } from './style';
import ToastComponent from '../../components/public/toast';
import NotificationBar from '../../components/public/notificationBar';
import Header from './components/header';
import Footer from './components/footer';
import { GlobalAction } from '../../actions';
import BreadcrumbWrapper from '../../components/public/breadcrumbWrapper';
import { crumbs } from './data';

const MainLayout = props => {
	const { children, notificationBar, showNotificationBar, hideNotificationBar } = props;
	const [showMenu, setShowMenu] = useState(false);
	const router = useRouter();

	const validBreadcrumb = [
		'/beats',
		'/texts',
		'/discounts',
		'/frequently-asked-questions',
		'/about-us',
		'/politics',
		'/rules',
		'/blog',
		'/beats/remakes',
	];

	useEffect(() => {
		if (showMenu) {
			toggleMenu();
		}
	}, [router.pathname]);

	const toggleMenu = () => setShowMenu(!showMenu);

	const toggleNotificationBar = () => {
		if (notificationBar.show) {
			hideNotificationBar();
		} else {
			showNotificationBar({ body: '' });
		}
	};

	return (
		<>
			<Head>
				<title>بیت باکس</title>
				<meta name="viewport" content="width=device-width, initial-scale=1" />
				<meta name="theme-color" content="#16171A" />
			</Head>
			<ContainerFluid>
				{showMenu && <MenuBackdrop onClick={toggleMenu} />}
				<ToastComponent />
				{notificationBar.show && (
					<NotificationBar
						body="ماموریت های انجام نشده ای دارید، لطفا آن ها را بررسی کنید."
						toggle={toggleNotificationBar}
					/>
				)}
				<Header toggleMenu={toggleMenu} showMenu={showMenu} showNotificationBar={notificationBar.show} />
				<div className={`main ${notificationBar.show ? 'main-move-to-bottom' : 'main-fixed'}`}>
					{validBreadcrumb.includes(router.pathname) && <BreadcrumbWrapper crumbs={crumbs} />}
					{children}
				</div>
				<Footer />
			</ContainerFluid>
		</>
	);
};

const mapStateToProps = state => {
	return {
		notificationBar: state.Global.notificationBar,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		showNotificationBar: payload => dispatch(GlobalAction.showNotificationBar()),
		hideNotificationBar: () => dispatch(GlobalAction.hideNotificationBar()),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(MainLayout);
