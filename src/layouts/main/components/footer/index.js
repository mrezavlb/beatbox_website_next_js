import Link from 'next/link';
import { Footer } from '../../style';
import { Col, Row } from 'reactstrap';
import { navbarList } from '../../data';
import certificateImg from '../../../../assets/img/png/enamad.png';
import { socialNetworkLinks } from '../../data';

const Index = () => {
	return (
		<Footer>
			<div className="container py-5">
				<Row>
					<Col className="main-links" sm={12} lg={3}>
						<ul className="main-links-list">
							{navbarList &&
								navbarList.map((item, key) => {
									if (item.id >= 6) {
										return (
											<li className="main-links-list-item" key={key}>
												<Link href={item.path}>
													<a className="main-links-list-link">{item.name}</a>
												</Link>
											</li>
										);
									}
								})}
						</ul>
					</Col>
					<Col className="about-us-info" sm={12} lg={6}>
						<h5 className="text-center text-white pb-2">درباره ما</h5>
						<span className="about-us-info-text">
							وبسایت بیت باکس از نخستین وبسایت ها در زمینه خدمات مورد نیاز هنرمندان صنعت موسیقی ایران است و خدماتی نظیر
							ساخت بیت، ترانه، میکس و مسترینگ و ضبط را به کاربران خود در این چند سال ارائه کرده است. این و بسایت در
							تاریخ ۲۴ خرداد سال ۱۳۹۲ فعالیت خود را آغاز کرد
						</span>
					</Col>
					<Col className="certificate" sm={12} lg={3}>
						<Link href="/">
							<a className="certificate-link" target="_blank">
								<img src={certificateImg} alt="certificate" />
							</a>
						</Link>
					</Col>
				</Row>
			</div>

			<div className="copyright">
				<div className="container copyright-wrapper">
					<div className="copyright-text">هر گونه کپی برداری بدون ذکر منبع مورد پیگرد قانونی قرار خواهد گرفت</div>
					<div>
						{socialNetworkLinks &&
							socialNetworkLinks.map(item => {
								return (
									<Link href={item.path} key={item.id}>
										<a className="social-network-link mr-3">
											<i className={`${item.icon} text-white`} />
										</a>
									</Link>
								);
							})}
					</div>
				</div>
			</div>
		</Footer>
	);
};

export default Index;
