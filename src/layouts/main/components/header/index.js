import Link from 'next/link';
import { Header } from '../../style';
import { useRouter } from 'next/router';
import { navbarList, userAccountLinks } from '../../data';
import appLogo from '../../../../assets/icons/svg/beatbox_website_logo.svg';
import menuIcon from '../../../../assets/icons/svg/menu_icon.svg';

const Index = props => {
	const { showMenu, toggleMenu, showNotificationBar } = props;
	const router = useRouter();

	return (
		<Header withNotification={showNotificationBar ? 'with-notification' : ''}>
			<div className="container h-100 d-flex justify-content-start align-items-center w-100">
				<div>
					<Link href="/">
						<a>
							<img src={appLogo} alt="" className="app-logo" />
						</a>
					</Link>
				</div>
				<nav className={`header-nav ${showMenu ? 'show-nav' : 'hide-nav'}`}>
					<ul className="header-nav-list">
						<li className="header-nav-responsive-logo">
							<Link href="/">
								<a>
									<img src={appLogo} alt="" className="app-logo" />
								</a>
							</Link>
						</li>
						{navbarList &&
							navbarList.map(item => {
								return (
									<li key={item.id} className="header-nav-list-item">
										<Link href={item.path}>
											<a
												className={`header-nav-list-link ${
													router.pathname === item.path
														? 'header-nav-list-link__active'
														: 'header-nav-list-link__inactive'
												}`}
												target={item.isTargetBlank && '_blank'}
											>
												{item.iconName !== '' && <i className={`${item.iconName} ml-2`} />}
												{item.name}
											</a>
										</Link>
									</li>
								);
							})}
					</ul>
				</nav>
				<div className="header-account-information">
					<ul className="header-account-information-list">
						{userAccountLinks &&
							userAccountLinks.map((item, key) => {
								return (
									<li className="header-account-information-list-item" key={key}>
										<Link href={item.path}>
											<a className="header-account-information-list-link" target={item.isTargetBlank && '_blank'}>
												<img
													src={item.iconPath}
													alt={item.name}
													className="header-account-information-list-item-icon"
												/>
											</a>
										</Link>
									</li>
								);
							})}
						<li className="app-menu-icon" onClick={toggleMenu}>
							<img src={menuIcon} alt="" className="" />
						</li>
					</ul>
				</div>
			</div>
		</Header>
	);
};

export default Index;
