import styled from 'styled-components';
import { pxToRem } from '../../helper/styleMixins';
import variables from '../../constants/styleVariables';

export const ContainerFluid = styled.div`
	width: 100%;
	padding: 0;
	margin: 0;
	background-color: ${variables.colors.AthensGray};

	.main {
		min-height: ${pxToRem(500)}rem;
	}

	.main-fixed {
		margin-top: ${pxToRem(62)}rem;
	}

	.main-move-to-bottom {
		margin-top: ${pxToRem(134)}rem;
	}
`;

export const MenuBackdrop = styled.div`
	width: 100%;
	height: 100%;
	position: fixed;
	top: 0;
	right: 0;
	z-index: 99;
	background-color: rgba(0, 0, 0, 0.85);
`;

export const Header = styled.header`
	width: 100%;
	height: ${pxToRem(62)}rem;
	background-color: ${variables.colors.black};
	position: fixed;
	right: 0;
	z-index: 999;

	${props => {
		if (props.withNotification === 'with-notification') {
			return `
                top: ${pxToRem(72)}rem;
          `;
		} else {
			return `
            	top: 0;
          `;
		}
	}}

	.app-logo {
		width: ${pxToRem(40)}rem;
		margin-left: ${pxToRem(20)}rem;
	}

	.app-menu-icon {
		width: ${pxToRem(30)}rem;
		margin-left: ${pxToRem(10)}rem;

		img {
			width: 100%;
		}
	}

	.show-nav {
		right: 0;
	}

	.hide-nav {
		right: -100%;
	}

	.header-nav {
		height: 100%;
		position: fixed;
		top: 0;
		width: 75%;
		background-color: ${variables.colors.Woodsmoke};
		z-index: 999;
		transition: 0.3s all;

		.header-nav-responsive-logo {
			display: flex;
			justify-content: center;
			align-items: center;
			padding: ${pxToRem(20)}rem;
			margin-bottom: ${pxToRem(20)}rem;
		}

		.header-nav-list {
			height: 100%;
			display: flex;
			justify-content: flex-start;
			flex-direction: column;

			.header-nav-list-item {
				width: 100%;
				transition: 0.2s all;
				padding: ${pxToRem(10)}rem;

				.header-nav-list-link {
					width: 100%;
					height: 100%;
					display: flex;
					justify-content: flex-start;
					align-items: center;
					font-size: ${variables.fontSize.base};
					padding-right: ${pxToRem(10)}rem;

					&__inactive {
						color: ${variables.colors.Silver};
					}

					&__active {
						color: ${variables.colors.white};
					}
				}

				&-icon {
					width: ${pxToRem(32)}rem;
				}

				&:hover {
					background-color: ${variables.colors.Danger};
				}
			}
		}
	}

	.header-account-information {
		display: flex;
		flex-direction: row-reverse;
		align-items: center;
		justify-content: end;
		flex-grow: 1;

		.header-account-information-list {
			height: 100%;
			display: flex;
			flex-direction: row-reverse;
			justify-content: flex-start;
			align-items: center;

			.header-account-information-list-item {
				width: ${pxToRem(46)}rem;
				transition: 0.2s all;

				&-icon {
					width: ${pxToRem(32)}rem;
				}
			}

			&-link {
				color: ${variables.colors.white};
				display: flex;
				justify-content: center;
				align-items: center;
				font-size: ${variables.fontSize.sm};
				padding: ${pxToRem(6)}rem;
			}
		}
	}

	@media (min-width: ${variables.breakPoints.laptop}px) {
		.app-menu-icon {
			display: none;
		}

		.show-nav {
			right: 0;
		}

		.hide-nav {
			right: 0;
		}

		.header-nav {
			position: relative;
			background-color: ${variables.colors.black};
			flex-grow: 2;

			.header-nav-responsive-logo {
				display: none;
			}

			.header-nav-list {
				flex-direction: row;

				.header-nav-list-item {
					width: ${pxToRem(110)}rem;
					padding: 0;

					.header-nav-list-link {
						justify-content: center;
						padding: 0;
						font-size: ${variables.fontSize.sm};
					}
				}
			}
		}
	}

	@media (min-width: ${variables.breakPoints.desktopSmall}px) {
		.header-nav {
			.header-nav-list {
				.header-nav-list-item {
					.header-nav-list-link {
						font-size: ${variables.fontSize.base};
					}
				}
			}
		}
	}
`;

export const Footer = styled.footer`
	width: 100%;
	background-color: ${variables.colors.MineShaft};

	.main-links {
		margin-bottom: ${pxToRem(40)}rem;

		.main-links-list {
			display: flex;
			flex-direction: column;

			.main-links-list-item {
				width: 100%;
				transition: 0.2s all;

				.main-links-list-link {
					width: 100%;
					height: 100%;
					padding: ${pxToRem(8)}rem;
					color: ${variables.colors.Silver};
					display: flex;
					justify-content: center;
					align-items: center;
					transition: 0.2s all;
					font-size: ${variables.fontSize.sm};

					&:hover {
						color: ${variables.colors.white};
					}
				}
			}
		}
	}

	.about-us-info {
		text-align: justify;
		color: ${variables.colors.Silver};

		.about-us-info-text {
			font-size: ${variables.fontSize.sm};
			line-height: 2.25;
		}
	}

	.certificate {
		display: flex;
		justify-content: center;
		align-items: center;
		margin-top: ${pxToRem(20)}rem;

		.certificate-link {
			background-color: ${variables.colors.WhiteLilac[0]};
			padding: ${pxToRem(12)}rem;
			border-radius: ${pxToRem(10)}rem;
		}
	}

	.copyright {
		width: 100%;
		height: auto;
		background-color: ${variables.colors.Woodsmoke};
		padding: ${pxToRem(20)}rem;
		font-size: ${variables.fontSize.sm};
		color: ${variables.colors.Silver};

		.copyright-wrapper {
			display: flex;
			justify-content: center;
			align-items: center;
			flex-direction: column;

			.copyright-text {
				text-align: center;
				margin-bottom: ${pxToRem(20)}rem;
			}

			.social-network-link {
				font-size: ${variables.fontSize.lg};
				color: ${variables.colors.white};
			}
		}
	}

	@media (min-width: ${variables.breakPoints.tabletWide}px) {
		.copyright {
			.copyright-wrapper {
				justify-content: space-between;
				flex-direction: row;

				.copyright-text {
					text-align: right;
					margin-bottom: 0;
				}
			}
		}

		.main-links {
			margin-bottom: 0;

			.main-links-list {
				.main-links-list-item {
					.main-links-list-link {
						justify-content: flex-start;
					}
				}
			}
		}
		.certificate {
			justify-content: flex-end;
			margin-top: 0;
		}
	}
`;
