import userAvatar from '../../../assets/icons/svg/user_avatar.svg';
import cartIcon from '../../../assets/icons/svg/cart_icon.svg';

export const navbarList = [
	{
		id: 1,
		path: '/',
		iconName: '',
		iconPath: '',
		name: 'خانه',
		isTargetBlank: false,
	},
	{
		id: 2,
		path: 'https://ribbon.studio/',
		iconName: '',
		iconPath: '',
		name: 'استودیو',
		isTargetBlank: true,
	},
	{
		id: 3,
		path: '/beats',
		iconName: '',
		iconPath: '',
		name: 'بیت ها',
		isTargetBlank: false,
	},
	{
		id: 4,
		path: '/texts',
		iconName: '',
		iconPath: '',
		name: 'تکست ها',
		isTargetBlank: false,
	},
	{
		id: 5,
		path: '/blog',
		iconName: '',
		iconPath: '',
		name: 'بلاگ',
		isTargetBlank: false,
	},
	{
		id: 6,
		path: '/rules',
		iconName: '',
		iconPath: '',
		name: 'قوانین',
		isTargetBlank: false,
	},
	{
		id: 7,
		path: '/politics',
		iconName: '',
		iconPath: '',
		name: 'سیاست ها',
		isTargetBlank: false,
	},
	{
		id: 8,
		path: '/about-us',
		iconName: '',
		iconPath: '',
		name: 'درباره ما',
		isTargetBlank: false,
	},
	{
		id: 9,
		path: '/frequently-asked-questions',
		iconName: '',
		iconPath: '',
		name: 'سوالات متداول',
		isTargetBlank: false,
	},
];

export const userAccountLinks = [
	{
		id: 1,
		path: '/my-account',
		iconPath: userAvatar,
		name: 'حساب من',
		isTargetBlank: false,
	},
	// {
	// 	id: 1,
	// 	path: '/cart',
	// 	iconPath: cartIcon,
	// 	name: 'سبد خرید',
	// 	isTargetBlank: false,
	// },
];

export const socialNetworkLinks = [
	{
		id: 1,
		path: '/',
		icon: 'fa fa-instagram',
		isTargetBlank: false,
	},
	{
		id: 2,
		path: '/',
		icon: 'fa fa-telegram',
		isTargetBlank: false,
	},
	{
		id: 3,
		path: '/',
		icon: 'fa fa-whatsapp',
		isTargetBlank: false,
	},
];

export const crumbs = [
	{
		id: 1,
		path: '/',
		name: 'خانه',
		crumb: 'home',
		isTargetBlank: false,
	},
	{
		id: 2,
		path: 'https://ribbon.studio/',
		name: 'استودیو',
		crumb: 'studio',
		isTargetBlank: true,
	},
	{
		id: 3,
		path: '/beats',
		name: 'بیت ها',
		crumb: 'beats',
		isTargetBlank: false,
	},
	{
		id: 4,
		path: '/texts',
		name: 'تکست ها',
		crumb: 'texts',
		isTargetBlank: false,
	},
	{
		id: 5,
		path: '/blog',
		name: 'بلاگ',
		crumb: 'blog',
		isTargetBlank: false,
	},
	{
		id: 6,
		path: '/rules',
		name: 'قوانین',
		crumb: 'rules',
		isTargetBlank: false,
	},
	{
		id: 7,
		path: '/politics',
		name: 'سیاست ها',
		crumb: 'politics',
		isTargetBlank: false,
	},
	{
		id: 8,
		path: '/about-us',
		name: 'درباره ما',
		crumb: 'about-us',
		isTargetBlank: false,
	},
	{
		id: 9,
		path: '/frequently-asked-questions',
		name: 'سوالات متداول',
		crumb: 'frequently-asked-questions',
		isTargetBlank: false,
	},
	{
		id: 10,
		path: '/discounts',
		name: 'تخفیف ها',
		crumb: 'discounts',
		isTargetBlank: false,
	},
	{
		id: 11,
		path: '/remakes',
		name: 'ریمیک ها',
		crumb: 'remakes',
		isTargetBlank: false,
	},
];
