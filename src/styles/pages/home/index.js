import styled from 'styled-components';
import { pxToRem } from '../../../helper/styleMixins';
import background from '../../../assets/img/jpg/home_background.jpg';
import variables from '../../../constants/styleVariables';

export const Section = styled.section`
	background-image: url(${background});
	background-repeat: no-repeat;
	background-position: center center;
	background-attachment: fixed;
	background-size: cover;
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: column;
	padding: ${pxToRem(20)}rem;

	${props => {
		if (props.withNotification === 'with-notification') {
			return `
                height: calc(100vh - ${pxToRem(134)}rem);
          `;
		} else {
			return `
            	height: calc(100vh - ${pxToRem(62)}rem);
          `;
		}
	}}

	.home-title {
		color: ${variables.colors.white};
		font-weight: ${variables.fontWeight.semibold};
	}

	.home-description {
		color: ${variables.colors.white};
	}
`;

export const Main = styled.main`
	//height: 500px;
	padding: ${pxToRem(50)}rem 0;
`;

export const Widget = styled.div`
	width: 100%;
	//background: ${variables.colors.white};
	border-radius: ${pxToRem(10)}rem;
	//height: 200px;
	padding: ${pxToRem(12)}rem ${pxToRem(16)}rem ${pxToRem(22)}rem ${pxToRem(16)}rem;

	.widget-title {
		font-size: ${variables.fontSize['3xl']};
		padding: ${pxToRem(8)}rem 0 ${pxToRem(16)}rem 0;
		border-bottom: 1px solid ${variables.colors.Silver};
	}
`;
