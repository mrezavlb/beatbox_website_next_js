import styled from 'styled-components';
import { pxToRem } from '../../../helper/styleMixins';
import variables from '../../../constants/styleVariables';

export const Detail = styled.section`
	width: 100%;
	background-color: ${variables.colors.white};
	height: 300px;
	border-radius: ${pxToRem(8)}rem;
`;

export const AudioWrapper = styled.div`
	display: flex;
	justify-content: flex-start;
	align-items: center;
	margin-top: ${pxToRem(20)}rem;

	.player {
		flex: 1 1 30%;
		margin-left: ${pxToRem(8)}rem;
	}
`;

export const BaseTicket = styled.div`
	min-width: ${pxToRem(100)}rem;
	background-color: ${variables.colors.white};
	padding: ${pxToRem(20)}rem ${pxToRem(10)}rem;
	border-radius: ${pxToRem(8)}rem;
	min-height: ${pxToRem(85)}rem;
	display: flex;
	align-items: center;
`;

export const NormalTicket = styled(BaseTicket)`
	justify-content: center;
	margin-left: ${pxToRem(8)}rem;
	font-size: ${variables.fontSize.sm};
	color: ${variables.colors.Woodsmoke};
`;

export const ExtraTicket = styled(BaseTicket)`
	flex: 1 1 10%;
	justify-content: space-between;
	background-color: ${variables.colors.Danger};
	cursor: pointer;

	.text-content {
		color: ${variables.colors.white};
		font-weight: ${variables.fontWeight.semibold};
	}

	.price {
		color: ${variables.colors.white};
		font-weight: ${variables.fontWeight.semibold};
	}
`;
