import styled from 'styled-components';
import { pxToRem } from '../../../helper/styleMixins';
import background from '../../../assets/img/jpg/home_background.jpg';
import variables from '../../../constants/styleVariables';

export const Widget = styled.div`
	width: 100%;
	border-radius: ${pxToRem(10)}rem;
	padding: ${pxToRem(12)}rem ${pxToRem(16)}rem ${pxToRem(22)}rem ${pxToRem(16)}rem;

	.widget-title {
		font-size: ${variables.fontSize['3xl']};
		padding: ${pxToRem(8)}rem 0 ${pxToRem(16)}rem 0;
		border-bottom: 1px solid ${variables.colors.Silver};
	}
`;

export const Filter = styled.div`
	width: auto;
	border-radius: ${pxToRem(10)}rem;
	padding: ${pxToRem(16)}rem;
	background-color: ${variables.colors.white};
	margin: ${pxToRem(12)}rem;
	display: flex;
	justify-content: flex-start;
	align-items: center;
`;

export const SearchBoxWrapper = styled.div`
	max-width: ${pxToRem(450)}rem;
	width: 100%;
	position: relative;
`;

export const SearchBox = styled.input`
	padding: ${pxToRem(10)}rem;
	width: 100%;
	border-radius: ${pxToRem(6)}rem;
	border: 1px solid ${variables.colors.Alto};

	&:focus {
		outline: none;
	}
`;

export const Submit = styled.div`
	position: absolute;
	top: 50%;
	left: ${pxToRem(12)}rem;
	transform: translate(0, -50%);
	padding: ${pxToRem(10)}rem;
	cursor: pointer;
`;
