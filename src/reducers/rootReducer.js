import { combineReducers } from 'redux';
import Post from './post';
import Global from './global';

const rootReducer = combineReducers({
	// reducers
	Post,
	Global,
});

export default rootReducer;
