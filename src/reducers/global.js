import * as actionTypes from '../constants/actionTypes';

export const initialState = {
	notificationBar: {
		show: false,
		body: '',
	},
};

const Global = (state = initialState, { type, payload }) => {
	switch (type) {
		case actionTypes.SHOW_NOTIFICATION_BAR:
			return {
				...state,
				notificationBar: { ...state.notificationBar, show: true, ...payload },
			};
		case actionTypes.HIDE_NOTIFICATION_BAR:
			return {
				...state,
				notificationBar: { show: false, body: '' },
			};

		default:
			return state;
	}
};

export default Global;
