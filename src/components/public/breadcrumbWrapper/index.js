import { Wrapper } from './style';
import Breadcrumbs from 'nextjs-breadcrumbs';
import { getBreadcrumbLabel } from '../../../helper/getBreadcrumbLabel';

const Index = ({ crumbs }) => {
	return (
		<Wrapper>
			<div className="container">
				<Breadcrumbs
					useDefaultStyle
					rootLabel="home"
					transformLabel={title => {
						return getBreadcrumbLabel(title, crumbs);
					}}
					containerClassName="breadcrumb-container"
				/>
			</div>
		</Wrapper>
	);
};

export default Index;
