import styled from 'styled-components';
import { pxToRem } from '../../../helper/styleMixins';
import variables from '../../../constants/styleVariables';

export const Wrapper = styled.section`
	background-color: ${variables.colors.MineShaft};
	height: ${pxToRem(120)}rem;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;

	.breadcrumb-container {
		ol {
			display: flex;
			justify-content: flex-start;
			align-items: center;

			li {
				display: flex;
				justify-content: flex-start;
				align-items: center;

				&:after {
					content: ' \\25C2';
					color: ${variables.colors.white};
					margin: 0 ${pxToRem(6)}rem 0 ${pxToRem(10)}rem;
				}

				&:hover {
					a {
						color: ${variables.colors.Silver};
					}
				}

				a {
					color: ${variables.colors.white};
					transition: 0.2s all;
				}
			}
		}
	}
`;
