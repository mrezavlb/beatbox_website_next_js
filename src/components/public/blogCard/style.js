import styled from 'styled-components';
import { pxToRem } from '../../../helper/styleMixins';
import variables from '../../../constants/styleVariables';

export const CardWrapper = styled.div`
	width: 100%;
	border-radius: ${pxToRem(10)}rem;
	box-shadow: 0 ${pxToRem(12)}rem ${pxToRem(50)}rem ${variables.colors.Silver};
	position: relative;
	overflow: hidden;
`;

export const Header = styled.div`
	width: 100%;
	height: ${pxToRem(200)}rem;

	.img {
		width: 100%;
		height: 100%;
	}
`;

export const Body = styled.div`
	width: 100%;
	min-height: ${pxToRem(150)}rem;
	padding: 0 ${pxToRem(8)}rem ${pxToRem(8)}rem ${pxToRem(8)}rem;

	.title {
		font-size: ${variables.fontSize.base};
		padding: ${pxToRem(10)}rem 0 ${pxToRem(4)}rem 0;
		font-weight: ${variables.fontWeight.semibold};
		transition: 0.2s all;
		color: ${variables.colors.black};
		display: flex;
		justify-content: flex-start;
		align-items: center;
	}

	.description {
		font-size: ${variables.fontSize.base};
		padding: ${pxToRem(8)}rem 0;
		font-weight: ${variables.fontWeight.normal};
		transition: 0.2s all;
		color: ${variables.colors.Scorpion};
		margin: 0;
	}
`;

export const Footer = styled.div`
	width: 100%;
	height: ${pxToRem(45)}rem;
	background-color: ${variables.colors.WhiteLilac[0]};
	border-radius: 0 0 ${pxToRem(10)}rem ${pxToRem(10)}rem;
	padding: ${pxToRem(8)}rem;
	display: flex;
	justify-content: space-between;
	align-items: center;

	.count-wrapper {
		display: flex;
		justify-content: flex-start;
		align-items: center;

		.count {
			color: ${variables.colors.Scorpion};
			display: flex;
			justify-content: flex-start;
			align-items: center;
			font-size: ${variables.fontSize.sm};
			margin-left: ${pxToRem(16)}rem;

			.fa {
				margin-left: ${pxToRem(4)}rem;
				color: ${variables.colors.DustyGray};
			}
		}
	}
`;

export const Writer = styled.div`
	width: 100%;
	color: ${variables.colors.Success};
	text-align: end;
	font-weight: ${variables.fontWeight.semibold};
`;
