import { Header, Body, Footer, LastPrice, Writer, CardWrapper } from './style';
import { persianNumber } from '../../../helper/convertNumber';

const Index = props => {
	const { writer, title, description, viewCount, commentCount, imgSrc } = props;
	return (
		<CardWrapper>
			<Header>
				<img src={imgSrc} alt={title} className="img" />
			</Header>
			<Body>
				<div className="title">{title}</div>
				<div className="description">{description}</div>
			</Body>
			<Footer className="footer">
				<div className="count-wrapper">
					<div className="count">
						<i className="fa fa-eye" /> {persianNumber(viewCount)}
					</div>
					<div className="count">
						<i className="fa fa-comments" /> {persianNumber(commentCount)}
					</div>
				</div>
				<Writer>{writer}</Writer>
			</Footer>
		</CardWrapper>
	);
};

export default Index;
