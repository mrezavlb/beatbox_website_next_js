import React from 'react';
import ReactPaginate from 'react-paginate';
import { PaginationWrapper } from './style';
import { persianNumber } from '../../../helper/convertNumber';

const Index = ({ totalPage, currentPage, pageChangeHandler }) => {
	return (
		<PaginationWrapper>
			<ReactPaginate
				pageCount={totalPage}
				pageRangeDisplayed={5}
				marginPagesDisplayed={1}
				forcePage={currentPage - 1}
				containerClassName={'pagination'}
				pageClassName={'page-item'}
				pageLinkClassName={'page-link'}
				activeClassName={'active-paginate-link'}
				previousLinkClassName={'page-link'}
				nextLinkClassName={'page-link'}
				previousClassName={'page-item'}
				nextClassName={'page-item'}
				previousLabel={'قبلی'}
				nextLabel={'بعدی'}
				onPageChange={selected => pageChangeHandler(selected)}
				pageLabelBuilder={item => {
					return <Label item={item} />;
				}}
			/>
		</PaginationWrapper>
	);
};

const Label = ({ item }) => {
	return <span>{persianNumber(item)}</span>;
};

export default Index;
