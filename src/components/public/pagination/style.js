import styled from 'styled-components';
import { pxToRem } from '../../../helper/styleMixins';
import variables from '../../../constants/styleVariables';

export const PaginationWrapper = styled.nav`
	width: ${pxToRem(300)}rem;
	margin: auto;

	ul {
		list-style: none;
		display: flex;
		justify-content: center;
		align-items: center;

		li {
			//width: ${pxToRem(50)}rem;
			//background: aliceblue;
			//padding: 10px;
			transition: 0.2s all;
			cursor: pointer;
			text-align: center;

			&:hover {
				//background-color: #34bea5;
			}

			&:first-child {
				.page-link {
					border-top-right-radius: 0.25rem;
					border-bottom-right-radius: 0.25rem;
					border-top-left-radius: 0;
					border-bottom-left-radius: 0;
				}
			}

			&:last-child {
				.page-link {
					border-top-left-radius: 0.25rem;
					border-bottom-left-radius: 0.25rem;
					border-top-right-radius: 0;
					border-bottom-right-radius: 0;
				}
			}

			.page-link {
				//padding: ${pxToRem(10)}rem;
				color: ${variables.colors.Danger};
				font-size: ${variables.fontSize.sm};
				border-radius: 0;
			}
		}

		.active-paginate-link {
			background-color: ${variables.colors.Danger};
			//border: 1px solid ${variables.colors.Danger};

			.page-link {
				background-color: transparent;
				color: ${variables.colors.white};
			}
		}
	}
`;
