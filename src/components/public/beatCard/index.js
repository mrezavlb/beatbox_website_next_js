import {
	// BeatCart,
	// DiscountTicket,
	// Title,
	// Price,
	// Genre,
	// Singer,
	// CardWrapper,
	Header,
	Body,
	Footer,
	LastPrice,
	NewPrice,
	CardWrapper,
} from './style';

const Index = props => {
	const { isDiscount, discount, title, imgSrc, price, lastPrice, type, genres, singer } = props;
	return (
		// <BeatCart>
		// 	{isDiscount && <DiscountTicket>٪۱۹</DiscountTicket>}
		// 	<img src={imgSrc} alt={title} />
		// 	<Genre>{genre}</Genre>
		// 	<div className="information">
		// 		<Title>
		// 			<span>{(type === 'beat' && 'بیت') || (type === 'text' && 'تکست')}</span>
		// 			<span className="mr-1">{title}</span>
		// 		</Title>
		// 		<Singer>{singer}</Singer>
		// 	</div>
		//
		// 	<Price> تومان {price}</Price>
		// </BeatCart>
		<CardWrapper>
			<Header>
				<img src={imgSrc} alt={title} className="img" />
			</Header>
			<Body>
				<div className="title">
					<div>{(type === 'beat' && 'بیت') || (type === 'text' && 'تکست')}</div>
					<div className="mr-1">{title}</div>
					{isDiscount && (
						<div className="discount-wrapper">
							<div className="discount">{discount}</div>
						</div>
					)}
				</div>
				<div className="singer">{singer}</div>
				<div className="genre-wrapper">
					{genres &&
						genres.map(item => {
							return (
								<div className="genre" key={item}>
									{item}
								</div>
							);
						})}
				</div>
			</Body>
			<Footer className="footer">
				{isDiscount && <LastPrice>{lastPrice}</LastPrice>}
				<NewPrice>{price} تومان </NewPrice>
			</Footer>
		</CardWrapper>
	);
};

export default Index;
