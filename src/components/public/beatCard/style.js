import styled from 'styled-components';
import { pxToRem } from '../../../helper/styleMixins';
import variables from '../../../constants/styleVariables';

// export const BeatCart = styled.div`
// 	width: 100%;
// 	height: ${pxToRem(266)}rem;
// 	border-radius: ${pxToRem(10)}rem;
// 	box-shadow: 0 ${pxToRem(12)}rem ${pxToRem(50)}rem ${variables.colors.Silver};
// 	position: relative;
// 	background-color: ${variables.colors.white};
// 	display: flex;
// 	justify-content: center;
// 	align-items: center;
//
// 	img {
// 		transition: 0.4s all;
// 		position: absolute;
// 		top: 0;
// 		right: 0;
// 		width: 100%;
// 		border-radius: ${pxToRem(10)}rem;
// 		height: calc(${pxToRem(266)}rem - ${pxToRem(16)}rem);
//
// 		&:hover {
// 			opacity: 0.8;
// 		}
// 	}
//
// 	.information {
// 		z-index: 10;
// 		display: flex;
// 		flex-direction: column;
// 		justify-content: center;
// 		align-items: center;
// 	}
//
// 	@media (min-width: ${variables.breakPoints.tabletWide}px) {
// 		width: ${pxToRem(266)}rem;
// 	}
// `;
//
// export const DiscountTicket = styled.div`
// 	width: ${pxToRem(80)}rem;
// 	height: ${pxToRem(30)}rem;
// 	background-color: ${variables.colors.Danger};
// 	position: absolute;
// 	top: -5%;
// 	left: ${pxToRem(10)}rem;
// 	border-radius: ${pxToRem(10)}rem;
// 	display: flex;
// 	justify-content: center;
// 	align-items: center;
// 	color: ${variables.colors.white};
// 	font-size: ${variables.fontSize.sm};
// 	z-index: 10;
// `;
//
// export const Title = styled.h3`
// 	font-size: ${variables.fontSize.base};
// 	padding: ${pxToRem(8)}rem 0;
// 	font-weight: ${variables.fontWeight.semibold};
// 	transition: 0.2s all;
// 	color: ${variables.colors.white};
// 	z-index: 10;
// 	margin: 0;
// 	display: flex;
// 	justify-content: center;
// 	align-items: center;
// `;
//
// export const Price = styled.div`
// 	width: 100%;
// 	position: absolute;
// 	bottom: 0;
// 	left: 0;
// 	background-color: ${variables.colors.BrightSun};
// 	border-radius: 0 0 ${pxToRem(10)}rem ${pxToRem(10)}rem;
// 	padding: ${pxToRem(8)}rem;
// 	display: flex;
// 	justify-content: center;
// 	align-items: center;
// 	color: ${variables.colors.black};
// `;
//
// export const Genre = styled.p`
// 	font-size: ${variables.fontSize.base};
// 	padding: ${pxToRem(8)}rem 0;
// 	font-weight: ${variables.fontWeight.semibold};
// 	transition: 0.2s all;
// 	z-index: 10;
// 	margin: 0;
// 	position: absolute;
// 	top: ${pxToRem(10)}rem;
// 	right: ${pxToRem(10)}rem;
// 	background-color: ${variables.colors.Alto};
// 	color: ${variables.colors.black};
// 	padding: ${pxToRem(5)}rem;
// 	border-radius: ${pxToRem(10)}rem;
// 	font-size: ${variables.fontSize.xs};
// `;
//
// export const Singer = styled.p`
// 	font-size: ${variables.fontSize.base};
// 	padding: ${pxToRem(8)}rem 0;
// 	font-weight: ${variables.fontWeight.semibold};
// 	transition: 0.2s all;
// 	color: ${variables.colors.white};
// 	z-index: 10;
// 	margin: 0;
// `;

export const CardWrapper = styled.div`
	width: 100%;
	min-height: ${pxToRem(266)}rem;
	border-radius: ${pxToRem(10)}rem;
	box-shadow: 0 ${pxToRem(12)}rem ${pxToRem(50)}rem ${variables.colors.Silver};
	position: relative;

	@media (min-width: ${variables.breakPoints.tabletWide}px) {
		width: ${pxToRem(266)}rem;
	}
`;

export const Header = styled.div`
	width: 100%;
	height: ${pxToRem(188)}rem;
	padding: ${pxToRem(8)}rem;

	.img {
		width: 100%;
		height: 100%;
		border-radius: ${pxToRem(10)}rem;
	}
`;

export const Body = styled.div`
	width: 100%;
	min-height: ${pxToRem(100)}rem;
	padding: 0 ${pxToRem(8)}rem ${pxToRem(8)}rem ${pxToRem(8)}rem;

	.title {
		font-size: ${variables.fontSize.base};
		padding: ${pxToRem(8)}rem 0;
		font-weight: ${variables.fontWeight.semibold};
		transition: 0.2s all;
		color: ${variables.colors.black};
		display: flex;
		justify-content: flex-start;
		align-items: center;
	}

	.singer {
		font-size: ${variables.fontSize.base};
		padding: ${pxToRem(8)}rem 0;
		font-weight: ${variables.fontWeight.semibold};
		transition: 0.2s all;
		color: ${variables.colors.black};
		z-index: 10;
		margin: 0;
	}

	.discount-wrapper {
		display: flex;
		justify-content: flex-end;
		align-items: center;
		flex-grow: 1;

		.discount {
			width: ${pxToRem(40)}rem;
			height: ${pxToRem(20)}rem;
			background-color: ${variables.colors.Danger};
			border-radius: ${pxToRem(10)}rem;
			display: flex;
			justify-content: center;
			align-items: center;
			color: ${variables.colors.white};
			font-size: ${variables.fontSize.sm};
			z-index: 10;
		}
	}

	.genre-wrapper {
		display: flex;
		justify-content: flex-start;
		align-items: center;
		flex-wrap: wrap;

		.genre {
			font-size: ${variables.fontSize.base};
			padding: ${pxToRem(8)}rem 0;
			font-weight: ${variables.fontWeight.semibold};
			transition: 0.2s all;
			margin: ${pxToRem(6)}rem 0 ${pxToRem(6)}rem ${pxToRem(6)}rem;
			background-color: ${variables.colors.Alto};
			color: ${variables.colors.black};
			padding: ${pxToRem(5)}rem;
			border-radius: ${pxToRem(10)}rem;
			font-size: ${variables.fontSize.xs};
		}
	}
`;

export const Footer = styled.div`
	width: 100%;
	height: ${pxToRem(45)}rem;
	background-color: ${variables.colors.WhiteLilac[0]};
	border-radius: 0 0 ${pxToRem(10)}rem ${pxToRem(10)}rem;
	padding: ${pxToRem(8)}rem;
	display: flex;
	justify-content: space-between;
	align-items: center;
`;

export const LastPrice = styled.del`
	width: 100%;
	color: ${variables.colors.Danger};
	text-align: start;
`;

export const NewPrice = styled.div`
	width: 100%;
	color: ${variables.colors.Success};
	text-align: end;
	font-weight: ${variables.fontWeight.semibold};
`;
