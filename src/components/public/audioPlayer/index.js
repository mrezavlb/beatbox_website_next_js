import { useState, useRef, useEffect } from 'react';
import { Control, Player } from './style';
import { persianNumber } from '../../../helper/convertNumber';

const Index = props => {
	const { path, isPlaying, setIsPlaying } = props;

	const [trackProgress, setTrackProgress] = useState(0);
	const [duration, setDuration] = useState(0);
	const [currentTime, setCurrentTime] = useState(0);
	const [localCurrentTime, setLocalCurrentTime] = useState('');
	const [localDurationTime, setLocalDurationTime] = useState('');

	const audioRef = useRef();
	const intervalRef = useRef();

	useEffect(() => {
		firstTimeLoad().then(() => null);
	}, []);

	useEffect(() => {
		if (isPlaying) {
			audioRef.current.play();
			startTimer();
		} else {
			clearInterval(intervalRef.current);
			audioRef.current.pause();
		}
	}, [isPlaying]);

	useEffect(() => {
		getAudioTime();
	}, [currentTime]);

	const firstTimeLoad = async () => {
		await setDuration(audioRef.current?.duration);
		await setCurrentTime(0);
		await setLocalCurrentTime('۰۰:۰۰:۰۰');
		if (audioRef.current?.duration) {
			await setLocalDurationTime(new Date(audioRef.current?.duration * 1000).toISOString().substr(11, 8));
		}
		await audioRef.current.load();
	};

	const getAudioTime = () => {
		const roundedCurrentTime = Math.floor(currentTime);

		if (roundedCurrentTime && duration) {
			setLocalCurrentTime(new Date(roundedCurrentTime * 1000).toISOString().substr(11, 8));
			setLocalDurationTime(new Date(duration * 1000).toISOString().substr(11, 8));
		}
	};

	const startTimer = () => {
		// Clear any timers already running
		clearInterval(intervalRef.current);

		intervalRef.current = setInterval(() => {
			if (audioRef.current.ended) {
				console.log('music ended');
				refreshTrack();
			} else {
				setTrackProgress(audioRef.current.currentTime);
				setCurrentTime(audioRef.current.currentTime);
			}
		}, [1000]);
	};

	const refreshTrack = () => {
		setIsPlaying(false);
	};

	const onScrub = value => {
		audioRef.current.currentTime = value;
		setTrackProgress(value);
	};

	const onScrubEnd = () => {
		// If not already playing, start
		if (!isPlaying) {
			setIsPlaying(true);
		}
		startTimer();
	};

	const currentPercentage = audioRef.current?.duration
		? `${(trackProgress / audioRef.current?.duration) * 100}%`
		: '0%';
	const trackStyling = `
    -webkit-gradient(linear, 0% 0%, 100% 0%, color-stop(${currentPercentage}, #fff), color-stop(${currentPercentage}, #777))
  `;

	return (
		<Player>
			<audio ref={audioRef}>
				<source src={path} type="audio/mpeg" />
			</audio>
			<div className="audio-player">
				{/*<AudioControls isPlaying={isPlaying} onPlayPauseClick={setIsPlaying} />*/}
				<div className="slider-wrapper">
					<div className="timer">
						<span>{persianNumber(localCurrentTime)}</span>
						<span>{persianNumber(localDurationTime)}</span>
					</div>
					<input
						type="range"
						value={trackProgress}
						step="1"
						min="0"
						max={duration}
						className="progress slider"
						onChange={e => onScrub(e.target.value)}
						style={{ background: trackStyling }}
						onMouseUp={onScrubEnd}
						onKeyUp={onScrubEnd}
					/>
				</div>
			</div>
		</Player>
	);
};

// const AudioControls = ({ isPlaying, onPlayPauseClick }) => {
// 	return (
// 		<Control>
// 			<div className="audio-controls">
// 				{isPlaying ? (
// 					<button type="button" className="pause" onClick={() => onPlayPauseClick(false)} aria-label="Pause">
// 						pause
// 					</button>
// 				) : (
// 					<button type="button" className="play" onClick={() => onPlayPauseClick(true)} aria-label="Play">
// 						play
// 					</button>
// 				)}
// 			</div>
// 		</Control>
// 	);
// };

export default Index;
