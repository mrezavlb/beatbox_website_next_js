import styled from 'styled-components';
import { pxToRem } from '../../../helper/styleMixins';
import variables from '../../../constants/styleVariables';

export const Player = styled.div`
	direction: ltr;
	//margin: 4px;

	.slider-wrapper {
		background-color: white;
		padding: ${pxToRem(20)}rem ${pxToRem(10)}rem;
		border-radius: ${pxToRem(10)}rem;
		position: relative;
		min-height: ${pxToRem(85)}rem;
		background-color: ${variables.colors.white};

		.timer {
			margin-bottom: ${pxToRem(10)}rem;
			width: 100%;
			display: flex;
			justify-content: space-between;
			align-items: center;
			font-size: ${variables.fontSize.sm};
			color: ${variables.colors.Woodsmoke};
		}

		.slider {
			height: ${pxToRem(5)}rem;
			//-webkit-appearance: none;
			width: 100%;
			margin-bottom: ${pxToRem(10)}rem;
			border-radius: ${pxToRem(8)}rem;
			background: #3b7677;
			transition: background 0.2s ease;
			cursor: pointer;
		}
	}
`;

export const Control = styled.div`
	.audio-controls {
		display: flex;
		justify-content: space-between;
		width: 75%;
		margin: 0 auto ${pxToRem(15)}rem;
	}

	.audio-controls .prev svg,
	.audio-controls .next svg {
		width: ${pxToRem(35)}rem;
		height: ${pxToRem(35)}rem;
	}

	.audio-controls .play svg,
	.audio-controls .pause svg {
		height: ${pxToRem(40)}rem;
		width: ${pxToRem(40)}rem;
	}

	.audio-controls path {
		fill: var(--white);
	}
`;
