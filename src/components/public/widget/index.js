import { Widget } from './style';
import { Col, Row } from 'reactstrap';
import Link from 'next/link';
import BeatCard from '../beatCard';
import backgroundImg from '../../../assets/img/jpg/home_background.jpg';
import Button from '../button';

const Index = props => {
	const { title, moreBtn, data, isDiscount, cardType } = props;
	return (
		<Widget>
			<h3 className="widget-title">{title}</h3>
			<Row className="mt-5 pt-3">
				{data &&
					data.map(item => {
						return (
							<Col sm={12} md={6} lg={4} xl={3} className="mb-5" key={item}>
								<Link href="/">
									<a>
										<BeatCard
											singer="اسم خواننده"
											genres={['هیپ هاپ', 'هیپ هاپ', 'هیپ هاپ']}
											type={cardType}
											isDiscount={isDiscount}
											discount="٪۱۹"
											title="این یک تایتل است"
											imgSrc={backgroundImg}
											price="۱۲۵۰۰۰"
											lastPrice="۱۲۵۰۰۰"
										/>
									</a>
								</Link>
							</Col>
						);
					})}
			</Row>
			{moreBtn.show && (
				<div className="d-flex justify-content-center align-items-center">
					<Button size="md" type="danger" onClick={() => null}>
						<Link href={moreBtn.path}>
							<a className="text-white">{moreBtn.content}</a>
						</Link>
					</Button>
				</div>
			)}
		</Widget>
	);
};

export default Index;
