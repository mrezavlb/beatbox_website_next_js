import styled from 'styled-components';
import { pxToRem } from '../../../helper/styleMixins';
import variables from '../../../constants/styleVariables';

export const Widget = styled.div`
	width: 100%;
	border-radius: ${pxToRem(10)}rem;
	padding: ${pxToRem(12)}rem ${pxToRem(16)}rem ${pxToRem(22)}rem ${pxToRem(16)}rem;

	.widget-title {
		font-size: ${variables.fontSize['3xl']};
		padding: ${pxToRem(8)}rem 0 ${pxToRem(16)}rem 0;
		border-bottom: 1px solid ${variables.colors.Silver};
	}
`;
