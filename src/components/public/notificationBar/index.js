import { Bar } from './style';

const Index = props => {
	const { body, toggle } = props;

	return (
		<Bar>
			<div className="body">
				{body}
				<i className="fa fa-close" onClick={toggle} />
			</div>
		</Bar>
	);
};

export default Index;
