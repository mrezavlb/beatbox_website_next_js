import styled from 'styled-components';
import { pxToRem } from '../../../helper/styleMixins';
import variables from '../../../constants/styleVariables';

export const Bar = styled.div`
	width: 100%;
	height: ${pxToRem(72)}rem;
	background-color: ${variables.colors.Woodsmoke};
	display: flex;
	justify-content: center;
	align-items: center;
	position: fixed;
	top: 0;
	right: 0;
	z-index: 999;
	padding: ${pxToRem(5)}rem;

	.body {
		width: auto;
		background-color: ${variables.colors.WhiteLilac[2]};
		display: flex;
		justify-content: center;
		align-items: center;
		color: ${variables.colors.black};
		padding: ${pxToRem(5)}rem;
		border-radius: ${pxToRem(5)}rem;

		.fa {
			color: ${variables.colors.Boulder};
			cursor: pointer;
			margin: 0 ${pxToRem(10)}rem;
		}
	}

	@media (min-width: ${variables.breakPoints.tabletWide}px) {
		.body {
			padding: ${pxToRem(10)}rem;
		}
	}
`;
