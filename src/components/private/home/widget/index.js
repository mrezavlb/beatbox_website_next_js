import { Widget } from '../../../../styles/pages/home';
import { Col, Row } from 'reactstrap';
import Link from 'next/link';
import BeatCard from '../../../public/beatCard';
import backgroundImg from '../../../../assets/img/jpg/home_background.jpg';
import Button from '../../../public/button';

const Index = props => {
	const { title, moreBtn, data, isDiscount, cardType } = props;
	return (
		<Widget>
			<h3 className="widget-title">{title}</h3>
			<Row className="mt-5 pt-3">
				{data &&
					data.map(item => {
						return (
							<Col sm={12} md={6} lg={4} xl={3} className="mb-5" key={item}>
								<Link href="/">
									<a>
										<BeatCard
											singer="اسم خواننده"
											genre="هیپ هاپ"
											type={cardType}
											isDiscount={isDiscount}
											title="این یک تایتل است"
											imgSrc={backgroundImg}
											price="۱۲۵۰۰۰"
										/>
									</a>
								</Link>
							</Col>
						);
					})}
			</Row>
			<div className="d-flex justify-content-center align-items-center">
				<Button size="md" type="danger" onClick={() => null}>
					<Link href="/">
						<a className="text-white">{moreBtn}</a>
					</Link>
				</Button>
			</div>
		</Widget>
	);
};

export default Index;
