import { Section } from '../../../../styles/pages/home';

const Index = props => {
	const { withNotification } = props;

	return (
		<Section withNotification={withNotification ? 'with-notification' : ''}>
			<h1 className="home-title mb-3">وبسایت بیت باکس</h1>
			<p className="home-description">
				خرید و سفارش با کیفیت ‌ترین بیت‌های اختصاصی دانلود رایگان بیت و دسترسی به مطالب آموزشی در زمینه موسیقی
			</p>
		</Section>
	);
};

export default Index;
