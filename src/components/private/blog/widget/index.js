import { Col, Row } from 'reactstrap';
import Link from 'next/link';
import BlogCard from '../../../public/blogCard';
import backgroundImg from '../../../../assets/img/jpg/home_background.jpg';
import { Widget } from '../../../../styles/pages/blog';

const Index = props => {
	const { data } = props;
	return (
		<Widget>
			<Row className="pt-3">
				{data &&
					data.map(item => {
						return (
							<Col sm={12} md={6} lg={4} className="mb-5" key={item}>
								<Link href="/">
									<a>
										<BlogCard
											title="این یک تایتل است"
											imgSrc={backgroundImg}
											writer="محمدرضا دهقانی"
											viewCount={5}
											commentCount={20}
											description="به عنوان یک فریلنسر، دانستن این موضوع که روی یک پروژه چقدر قیمت بگذارید می‌تواند حسی مانند شرط بندی باشد."
										/>
									</a>
								</Link>
							</Col>
						);
					})}
			</Row>
		</Widget>
	);
};

export default Index;
