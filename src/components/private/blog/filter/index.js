import { Filter, SearchBox, SearchBoxWrapper, Submit } from '../../../../styles/pages/blog';

const Index = props => {
	const {} = props;

	return (
		<Filter>
			<SearchBoxWrapper>
				<SearchBox type="text" />
				<Submit>
					<i className="fa fa-search" />
				</Submit>
			</SearchBoxWrapper>
		</Filter>
	);
};

export default Index;
