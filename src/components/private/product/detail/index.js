import { Detail } from '../../../../styles/pages/product';

const Index = props => {
	const { productId } = props;

	return <Detail>محصول {productId}</Detail>;
};

export default Index;
