import { AudioWrapper, NormalTicket, ExtraTicket } from '../../../../styles/pages/product';
import AudioPlayer from '../../../public/audioPlayer';
import { persianNumber } from '../../../../helper/convertNumber';

const Index = props => {
	const { musicPath, isPlaying, setIsPlaying, productId } = props;

	return (
		<AudioWrapper>
			<NormalTicket>زمان : {persianNumber('03:35')}</NormalTicket>

			<NormalTicket>کیفیت : {persianNumber('320')}</NormalTicket>
			<NormalTicket>حجم : {persianNumber('320')}</NormalTicket>
			<div className="player">
				<AudioPlayer path={musicPath} isPlaying={isPlaying} setIsPlaying={setIsPlaying} />
			</div>
			<ExtraTicket>
				<span className="text-content">
					<i className="fa fa-shopping-cart" /> اضافه به سبد خرید
				</span>
				<span className="price">{`${persianNumber(300000)} تومان `}</span>
			</ExtraTicket>
		</AudioWrapper>
	);
};

export default Index;
