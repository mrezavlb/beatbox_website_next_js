import { apiCaller } from '../helper/apiCaller';
import { convertJsonToQueryString } from '../helper/convertJsonToQueryString';

export const getPostsService = postData => {
	return apiCaller().get(`${process.env.NEXT_APP_API_URL}/posts?${convertJsonToQueryString(postData)}`);
};

export const getPostDetailsService = postData => {
	return apiCaller().get(`${process.env.NEXT_APP_API_URL}/posts/${postData}`);
};
