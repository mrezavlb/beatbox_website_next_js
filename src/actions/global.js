import * as actionTypes from '../constants/actionTypes';

export const showNotificationBar = payload => {
	return {
		type: actionTypes.SHOW_NOTIFICATION_BAR,
		payload,
	};
};
export const hideNotificationBar = () => {
	return {
		type: actionTypes.HIDE_NOTIFICATION_BAR,
	};
};
