import * as PostAction from './post';
import * as GlobalAction from './global';

export { PostAction, GlobalAction };
