import { toast } from 'react-toastify';
import * as actionTypes from '../constants/actionTypes';
import * as postApi from '../apis/post';


export const getPosts = (postData) => {
    return async (dispatch) => {
        dispatch({ type: actionTypes.GET_POSTS_REQUEST });
        try {
            const response = await postApi.getPostsService(postData);
            const data = await response?.data;
            dispatch({ type: actionTypes.GET_POSTS_SUCCESS, payload: data });
        } catch (error) {
            console.log(error);
            toast.error('Error!');
            dispatch(getPostsFailure());
        }
    };
};
export const getPostsFailure = () => {
    return {
        type: actionTypes.GET_POSTS_FAILURE,
    };
};

